"""
The folder contains user-written test cases grouped into campaign classes
Example:

from jugis.report import CampaignBase
from jugis.manager import Manager

from your_project.test.connectivity import test_connection, test_disconnection
from your_project.test.corrupted_packets import test_corrupted_multicast, test_corrupted_unicast


class CampaignDataTransfer(CampaignBase):
    name = 'Data Transfer'
    def collect_tests(self) -> List[Callable]:
        return [
            test_connection,
            test_disconnection,
            test_corrupted_multicast,
            test_corrupted_unicast
        ]

if __name__ == '__main__':
    manager = Manager()
    manager.config.report.DO_SEND_REPORTS = True
    manager.start_campaign(CampaignDataTransfer())
"""
