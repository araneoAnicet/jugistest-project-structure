"""
This folder contains structured data schemas
Example:

from jugis.config_schema import BaseDeviceConfig, SshTTYConfigSchema

from your_project.schemas.power_triggers import MqttPowerTriggerSchema


class CiscoRouterDeviceSchema(BaseDeviceConfig):
    tty: SshTTYConfigSchema
    power_trigger: MqttPowerTriggerSchema

"""
