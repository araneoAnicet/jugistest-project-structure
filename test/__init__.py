"""
The folder contains the actual test scenarios which are then collected in a test campaign
Example:

import jugis


@jugis.report.mark.documentation(description='This test always passes')
def test_always_passing():
    assert True

"""