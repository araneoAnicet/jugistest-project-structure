"""
This folder contains fixtures for the project.
Example:

import jugis

from your_project.devices import CiscoRouter


@jugis.report.fixture(scope='session')
def cisco_router():
    device = CiscoRouter(YOUR_CONFIG)
    device.build()
    device.initialize()
    yield device
    device.finalize()
"""
