"""
The folder contains user-defined classes for the devices
Example:

from jugis.device import BaseDevice

from your_project.utilities.device.power_triggers import MqttPowerTrigger
from your_project.utilities.device.tty import CiscoSshTTY
from your_project.schemas.cisco import CiscoRouterDeviceSchema

class CiscoRouter(BaseDevice):
    def build(self)
        self.config = CiscoRouterDeviceSchema(self.config)
        self.power_trigger = MqttPowerTrigger(self.config)
        self.tty = CiscoSshTTY(self.config)
"""
