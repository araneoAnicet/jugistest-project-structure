"""
The folder contains utilities used by the tests.
Example:
import re
from typing import Tuple, List, Any

from jugis.helpers import CRUDHelper, OnOffHelper, ListHelper, SetterHelper, ErrorsHandler
from jugis.tty import ExpectPromptInterface



class GeneralLinuxErrorsHandler(ErrorsHandler):
    errors_list = ['never heard of command', 'Operation not permitted', "can't create bridge with the same name"]


class BrctlBridgePriority(SetterHelper):
    errors_handler = GeneralLinuxErrorsHandler()
    _brctl = 'brctl {}'
    _brctl_set_bridge_prio = _brctl.format('setbridgeprio {} {}')

    def __init__(self, name: str):
        self.name = name

    def _set(self, tty, value) -> str:
        return tty.send_expect_prompt(self._brctl_set_bridge_prio.format(self.name, value))

    def _get(self, tty) -> Tuple[str, Any]:
        output = tty.send_expect_prompt(self._brctl.format(f'show {self.name}'))
        pattern = fr'{self.name}\s+([0-9a-f]+)\.[0-9a-f]+\s+(yes|no)\s+'
        match_object = re.search(pattern, output)
        assert match_object, f'No pattern matching {pattern} is found'
        return output, int(f'0x{match_object.group(1)}', 0)

"""